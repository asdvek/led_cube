// TODO: Add buttons for changing mode (or rotatry encoder, oled, something else?)
// TODO: Add more animations (fluid simulation in real time (or cached, whatever works))

const float pi = 3.14159265f;

const byte latchPin = A1;         // pin connected to ST_CP of 74HC595
const byte clockPin = A2;         // pin connected to SH_CP of 74HC595
const byte dataPin = A0;          // pin connected to DS    of 74HC595
const byte n_registers = 8;       // number of shift registers in use
const byte n_layers = 8;          // number of layers in the cube
const byte brightness_steps = 6; // number of different brightness levels

// define cube layer pins
int layer_pins[] = {5, 6, 7, 8, 9, 10, 11, 12};              // pins for controlling transistors for grounding each cathode layer

byte frame[n_layers][n_registers] = {{0}};  // holds states of all leds. each register gets a byte that corresponds to the states of 8 leds in a row

// temporary variables for debugging
int counter = 0;

void setup() {
  // disable interrupts
  cli();

  // set timer1 to interrupt at 8*60 Hz
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;  // init counter to 0
  // set compare match register for 60hz increments
  // OCR1A = 33332; // = (16*10^6) / (1*8) - 1 (must be <65536)
  OCR1A = 2499; // = (16*10^6) / (1*8) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 bit for 8 prescaler
  TCCR1B |= (1 << CS11);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);


  // enable interrupts
  sei();
  
  
  // PC connection
  Serial.begin(57600);
  // Serial.println("Serial connection opened.");
  
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  // init catchode layer transistors
  for (int i = 0; i < n_layers; i++)
  {
    pinMode(layer_pins[i], OUTPUT);
  }

  pinMode(A7, INPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);

  // tmp for debugging
  pinMode(13, OUTPUT);

  // encoder interrupts
  attachInterrupt(digitalPinToInterrupt(2), button_pressed, RISING);
  // attachInterrupt(digitalPinToInterrupt(3), encoder_turned, FALLING);

  // send first ACK to signify that arduino can start accepting data
  Serial.print((char)6);
}

int current_layer = 0;
int old_layer;
int interrupt_threshold = 1;
int value = 0;

void button_pressed()
{
  Serial.println("Button pressed!");
}

// interrupt service routine for timer1
ISR(TIMER1_COMPA_vect)
{
  // switch cube layer that is turned on
  if (current_layer >= 8) current_layer = 0;


  // take the latchPin low so 
  // the LEDs don't change while bits are being sent
  fastDigitalWrite(1, LOW);
  
  // shift out the bits:
  for (int current_register = n_registers-1; current_register >= 0; current_register--)
  {
    fastShiftOut(frame[current_layer][current_register]);
  }

  //take the latch pin high so the LEDs will light up
  fastDigitalWrite(1, HIGH);

  select_layer(current_layer, layer_pins, n_layers);

  current_layer++;
}


void loop()
{  
  // clear_cube();
  read_cube();
  delay(50);
}

// reads cube state from serial
void read_cube()
{
  if (Serial.available())
  {
    digitalWrite(13, LOW);  // reset read fail led

    // read first half of the data
    for (int z = 0; z < 4; z++)
    {
      for (int y = 0; y < 8; y++)
      {
        int read_byte = Serial.read();

        // byte read failed
        if (read_byte == -1)
        {
          read_byte = 0;
          digitalWrite(13, HIGH);
        }
        
        frame[z][y] = read_byte;
      }
    }

    flush_serial_buffer();
    Serial.write(5);  // ask pc for the second half of the data

    // wait for the second half of the data to arrive
    while (Serial.available() < 32);
  
    // read first half of the data
    for (int z = 4; z < 8; z++)
    {
      for (int y = 0; y < 8; y++)
      {
        int read_byte = Serial.read();

        // byte read failed
        if (read_byte == -1)
        {
          read_byte = 0;
          digitalWrite(13, HIGH);
        }
        
        frame[z][y] = read_byte;
      }
    }

    Serial.write(6);  // tell pc that all data has been successfully read
  }
}

void flush_serial_buffer()
{
  while (Serial.available()) Serial.read();
}

void clear_cube()
{
  for (int layer = 0; layer < n_layers; layer++)
  {
    for (int reg = 0; reg < n_registers; reg++)
    {
      frame[layer][reg] = 0;
    }
  }
}

// manipulates frame array to turn led at x, y, z on
void led_on(byte frame[][n_registers], int x, int y, int z)
{
  frame[z][y] |= (1 << x);
}

void led_off(byte frame[][n_registers], int x, int y, int z)
{
  frame[z][y] &= ~(1 << x);
}

void xz_on(int y)
{
  for (int x = 0; x < 8; x++)
  {
    for (int z = 0; z < 8; z++)
    {
      led_on(frame, x, y, z);
    }
  }
}

void toggle_led(byte frame[][n_registers], int x, int y, int z)
{
  frame[z][y] ^= (1 << x);
}


// layer is a value in range [0, 3]
void select_layer(int layer_index, int layer_pins[], int n_layers)
{
  // turn all layers off
  for (int i = 0; i < n_layers; i++)
  {
    digitalWrite(layer_pins[i], LOW);
  }

  // turn desired layer on
  digitalWrite(layer_pins[layer_index], HIGH);
}

// pin takes values in the range [0, 7] which correspond to the analog pins
void fastDigitalWrite(int pin, int state)
{
  // clear pin to be accessed
  PORTC &= ~(1 << pin);

  // set pin to requested state
  PORTC ^= state << pin;
}

// data pin hard coded to A0
// clock pin hard coded to A2
// hard coded to work as LSBFIRST
void fastShiftOut(byte data)
{
  for (int current_bit = 0; current_bit < 8; current_bit++)
  {
    // set clock to low and data to right value
    PORTC &= ~0b00000100; // set clock to low
    fastDigitalWrite(0, (data >> current_bit) & 0x01);

    PORTC ^= 0b00000100; // set clock to high
  }
  PORTC &= ~0b00000100; // set clock to low
}
