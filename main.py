import serial
from math import sqrt, sin, cos, pi
from datetime import datetime
import numpy as np
import struct


class Particle:
    def __init__(self, pos):
        self.pos = pos
        self.v = np.array([0.0, 0.0, 0.0])
        self.a = np.array([0.0, 0.0, 0.0])
        self.m = 1
        self.F = np.array([0.0, 0.0, 0.0])

    def update(self, dt):
        self.a = self.F/self.m
        self.v += self.a*dt
        self.pos += self.v*dt


class Particles:
    def __init__(self):
        self.particles = []
        for i in range(20):
            pos = np.random.random(3) * 8
            p = Particle(pos)
            p.v = (np.random.random(3)*2 - 1)*5 + 15
            p.v[0] = 0
            p.v[1] = 0
            self.particles.append(p)

    def compute_frame(self, t, dt):
        # clear frame
        frame = init_frame()

        # return empty frame if too long time step is assumed (prevents startup lag from messing up the simulation)
        if dt > 0.10:
            return frame

        # update particle positions
        for p in self.particles:
            p.update(dt)

            if p.pos[0] < 0:
                p.pos[0] = 0
                p.v[0] = -p.v[0]
            if p.pos[0] > 8:
                p.pos[0] = 7.9
                p.v[0] = -p.v[0]

            if p.pos[1] < 0:
                p.pos[1] = 0
                p.v[1] = -p.v[1]
            if p.pos[1] > 8:
                p.pos[1] = 7.9
                p.v[1] = -p.v[1]

            if p.pos[2] < 0:
                p.pos[2] = 0
                p.v[2] = -p.v[2]
            if p.pos[2] > 8:
                p.pos[2] = 7.9
                p.v[2] = -p.v[2]

        # draw particles
        for p in self.particles:
            if 0 <= p.pos[0] < 8 and 0 <= p.pos[1] < 8 and 0 <= p.pos[2] < 8:
                frame[int(p.pos[2])][int(p.pos[1])][int(p.pos[0])] = 1

        return frame


class Wave:
    def __init__(self):
        pass

    def compute_frame(self, t, dt):
        frame = init_frame()

        for x in range(8):
            for y in range(8):
                x_offset = 3.5
                y_offset = 3.5
                dx = x - x_offset
                dy = y - y_offset
                r = sqrt(dx ** 2 + dy ** 2)

                z = (sin(2*pi*t - 0.6*r) + 1) / 2 * 8

                frame[int(z)][y][x] = 1
        return frame


class RotatingWave:
    def __init__(self):
        pass

    def compute_frame(self, t, dt):
        frame = init_frame()

        for x in range(8):
            for y in range(8):
                f = 0.1
                x_offset = 8 * (cos(2 * pi * f * t) + 1) / 2
                y_offset = 8 * (sin(2 * pi * f * t) + 1) / 2
                dx = x - x_offset
                dy = y - y_offset
                r = sqrt(dx ** 2 + dy ** 2)

                z = (sin(2 * pi * 0.5 * t - 0.5 * r) + 1) / 2 * 8

                frame[int(z)][y][x] = 1
        return frame


class Rain:
    def __init__(self):
        self.particles = []

    def compute_frame(self, t, dt):
        # clear frame
        frame = init_frame()

        # spawn droplets
        for _ in range(np.random.randint(3) + 1):
            pos = np.random.random(3) * 8
            pos[2] = 9
            p = Particle(pos)

            p.v = np.array([0.0, 0.0, -20.0])
            p.F = np.array([0.0, 0.0, -9.81])
            self.particles.append(p)

        # update particle positions
        for p in self.particles:
            p.update(dt)

        # remove particles that are off screen
        del_indices = []
        for i in range(len(self.particles)):
            if self.particles[i].pos[2] < 0:
                del_indices.append(i)
        for i in reversed(del_indices):  # loop through list in reverse so last elements are deleted first
            del self.particles[i]

        # draw particles
        for p in self.particles:
            if 0 <= p.pos[0] < 8 and 0 <= p.pos[1] < 8 and 0 <= p.pos[2] < 8:
                frame[int(p.pos[2])][int(p.pos[1])][int(p.pos[0])] = 1

        return frame


class Cube:
    def __init__(self):
        # vertices of a cube with side length of 2 in object space (CG of cube at the origin)
        # coordinates are expressed as 4-vectors with components [x, y, z, w]
        self.vertices = np.array([
            # lower vertices
            np.array([ 1, -1, -1,  1], dtype=np.float32),
            np.array([ 1,  1, -1,  1], dtype=np.float32),
            np.array([-1,  1, -1,  1], dtype=np.float32),
            np.array([-1, -1, -1,  1], dtype=np.float32),
            # upper vertices
            np.array([ 1, -1,  1,  1], dtype=np.float32),
            np.array([ 1,  1,  1,  1], dtype=np.float32),
            np.array([-1,  1,  1,  1], dtype=np.float32),
            np.array([-1, -1,  1,  1], dtype=np.float32),
        ])

        # list of 4-tuples containing the indices of the vertices belonging to a face quad
        self.faces = [(0, 1, 2, 3), (0, 1, 5, 4), (1, 2, 6, 5), (2, 3, 7, 6), (3, 0, 4, 7), (4, 5, 6, 7)]

        # state vectors of the cube
        self.pos = np.array([3.5, 3.5, 3.5, 1], dtype=np.float32)   # position vector
        self.R = np.array([0, 0, 0], dtype=np.float32)              # vector of euler angles (yaw, pitch, roll)
        self.s = np.array([0.5, 0.5, 0.5], dtype=np.float32)        # scaling vector in local coordinates (object space)

        self.frame = init_frame()

    def compute_frame(self, t, dt):
        self.frame = init_frame()  # clear frame

        # update orientation
        self.R += np.array([5, 2, 0], dtype=np.float32)*dt

        # update scale
        scale = (sin(2*pi*t)+1)/2*2.5+0.5
        self.s = np.array([scale, scale, scale], dtype=np.float32)

        # compute modelview matrix
        S = scale_matrix(self.s)
        R = rotation_matrix(self.R[0], self.R[1], self.R[2])
        T = translation_matrix(self.pos)
        modelview = T @ R @ S  # scale first, then rotate and translate

        # compute cube vertices in world space, i.e. transform the vertices from object space to
        # world space using the modelview matrix computed earlier
        world_vertices = []
        for v in self.vertices:
            v_world = modelview @ v
            world_vertices.append(v_world)

        # draw lines at face edges for vertices in world space
        for f in self.faces:
            for i in range(len(f)):
                v1 = world_vertices[f[i-1]]
                v2 = world_vertices[f[i]]
                self.draw_line(v1, v2)

        return self.frame

    # v1 and v2 are vertices between which the line is drawn
    def draw_line(self, v1, v2):
        dv = v2 - v1  # vector from v1 to v2
        dx = abs(dv[0])
        dy = abs(dv[1])
        dz = abs(dv[2])

        # if vertices are coincident display a point
        if np.all(v1.astype(int) == v2.astype(int)):
            v1 = v1.astype(int)
            self.frame[v1[2]][v1[1]][v1[0]] = 1
            return

        # find out what component of v is the largest and compute values for the other components.
        # the range of the largest component is looped over as opposed to the shorter ones to make sure
        # that there are no gaps in the line. If two or more of the components are equal and the largest,
        # it doesn't make a difference which one of them is looped over.

        # if dx is one of the longest
        if dx >= dy and dx >= dz:
            if v2[0] < v1[0]:
                v1, v2 = (v2, v1)
                dv = v2-v1

            for x_ in range(int(dx)+1):
                # scale dv for all different x values
                v = v1 + dv*x_/dx

                # get other components
                x = int(v1[0]) + x_
                y = int(v[1])
                z = int(v[2])
                if 0 <= x < 8 and 0 <= y < 8 and 0 <= z < 8:
                    self.frame[z][y][x] = 1
        # if dy is one of the longest:
        elif dy >= dx and dy >= dz:
            if v2[1] < v1[1]:
                v1, v2 = (v2, v1)
                dv = v2-v1

            for y_ in range(int(dy)+1):
                # scale dv for all different y values
                v = v1 + dv * y_ / dy

                # get other components
                x = int(v[0])
                y = int(v1[1])+y_
                z = int(v[2])
                if 0 <= x < 8 and 0 <= y < 8 and 0 <= z < 8:
                    self.frame[z][y][x] = 1
        # if dz is one of the longest:
        elif dz >= dx and dz >= dy:
            if v2[2] < v1[2]:
                v1, v2 = (v2, v1)
                dv = v2-v1

            for z_ in range(int(dz)+1):
                # scale dv for all different z values
                v = v1 + dv * z_ / dz

                # get other components
                x = int(v[0])
                y = int(v[1])
                z = int(v1[2])+z_
                if 0 <= x < 8 and 0 <= y < 8 and 0 <= z < 8:
                    self.frame[z][y][x] = 1


class Sphere:
    def __init__(self, pos, r_max):
        self.pos = pos
        self.r_max = r_max
        self.r = None

    def compute_frame(self, t, dt):
        frame = init_frame()

        self.r = self.r_max  # (sin(2*pi*t)+1)/2*self.r_max

        for z in range(8):
            for y in range(8):
                for x in range(8):
                    r_test = int(sqrt((self.pos[0]-x)**2 + (self.pos[1]-y)**2 + (self.pos[2]-z)**2))
                    if r_test == self.r:
                        frame[z][y][x] = 1

        return frame


class Spiral:
    class Dot:
        def __init__(self, r, f, f_z, t_offset):
            self.r = r
            self.f = f
            self.f_z = f_z
            self.t_offset = t_offset

        def draw(self, frame, t):
            z = (cos(2 * pi * self.f_z * t - self.t_offset) + 1) / 2 * 8
            x = 4 + cos(2 * pi * self.f * t  - self.t_offset)*self.r
            y = 4 + sin(2 * pi * self.f * t - self.t_offset)*self.r
            frame[int(z)][int(y)][int(x)] = 1

    def __init__(self):
        self.frame = init_frame()
        self.dots = list()
        for _ in range(30):
            self.dots.append(Spiral.Dot(np.random.randint(4)+1, 0.5 + np.random.random()/2,
                                        0.1 + 0.1*np.random.random(), np.random.random()*4))

    def compute_frame(self, t, dt):
        self.frame = init_frame()
        for dot in self.dots:
            dot.draw(self.frame, t)
        return self.frame


# TODO: paddle controls
# TODO: side collisions
# TODO: scoring
class Pong:
    class Paddle:
        # pos       = position vector to a corner of the paddle
        # size      = vector denoting the size of the paddle [width, height]
        # normal    = surface normal vector of the paddle plane
        def __init__(self, pos, size, normal):
            self.pos = pos
            self.size = size
            self.normal = normal

            # shortcuts for components of position and size vectors
            self.x, self.y, self.z = None, None, None
            self.w, self.h = None, None
            self.compute_locals()

        def move(self, disp):
            if np.linalg.norm(disp) != 1:
                raise RuntimeError("Paddle displacement vector must be unit length")
            self.pos += disp
            self.compute_locals()
            self.normal = normal

        def draw(self, frame):
            for x in range(self.w):
                for z in range(self.h):
                    frame[self.z + z][self.y][self.x + x] = 1

        def compute_locals(self):
            self.x = self.pos[0]
            self.y = self.pos[1]
            self.z = self.pos[2]
            self.w = self.size[0]
            self.h = self.size[1]

    class Ball:
        def __init__(self, pos, v):
            self.last_pos = None  # used for collision detection
            self.pos = pos
            self.v = v

        def update(self, dt):
            self.pos += self.v*dt

        def draw(self, frame):
            if 0 <= self.pos[0] < 8 and 0 <= round(self.pos[1]) < 8 and 0 <= self.pos[2] < 8:
                frame[int(self.pos[2])][int(round(self.pos[1]))][int(self.pos[0])] = 1

    def __init__(self):
        self.p1 = Pong.Paddle(np.array([2, 0, 2]), np.array([4, 4]), np.array([0, 1, 0]))
        self.p2 = Pong.Paddle(np.array([2, 7, 2]), np.array([4, 4]), np.array([0, -1, 0]))
        self.b  = Pong.Ball(np.array([3.5, 3.5, 3.5], dtype=np.float32), np.array([0, -5, 0], dtype=np.float32))

    def compute_frame(self, t, dt):
        frame = init_frame()

        # update ball
        self.b.update(dt)

        # check for collisions
        self.collisions()

        # print(self.b.pos)

        # draw paddles
        self.p1.draw(frame)
        self.p2.draw(frame)
        self.b.draw(frame)

        self.b.last_pos = np.copy(self.b.pos)  # update ball last frame position
        return frame

    # handle collisions
    def collisions(self):
        # ignore collisions for first the first frame
        if self.b.last_pos is None:
            return

        # check if the trajectory of the ball intersects with the first paddle
        # compute collision point with plane of paddle 1
        coll_p1 = self.vector_plane_collision(self.b.pos, self.b.v, self.p1.y + 1)
        # check if collision point was within the paddle
        if self.p1.x <= coll_p1[0] <= self.p1.x + self.p1.w and self.p1.z <= coll_p1[2] <= self.p1.z + self.p1.h:
            # check if the ball was at the other side of the paddle at last timestep
            collision_test = np.dot((self.b.pos - self.b.last_pos), (coll_p1 - self.b.pos))

            # print(f"change in position: {self.b.pos-self.b.last_pos}, collision: {collision_test}")
            if collision_test < 0 and np.dot(self.b.v, self.p1.normal) < 0:
                # move ball to collision point and reflect velocity from paddle plane
                self.b.pos = coll_p1
                self.b.last_pos = np.copy(self.b.pos)  # reset old pos so that paddle2 collision doesn't trigger
                print("collision with paddle 1")

                self.b.v += -2*np.dot(self.b.v, self.p1.normal)*self.p1.normal

        # check if the trajectory of the ball intersects with the second paddle
        # compute collision point with plane of paddle 2
        coll_p2 = self.vector_plane_collision(self.b.pos, self.b.v, self.p2.y - 1)
        # check if collision point was within the paddle
        if self.p2.x <= coll_p2[0] <= self.p2.x + self.p2.w and self.p2.z <= coll_p2[2] <= self.p2.z + self.p2.h:
            # check if the ball was at the other side of the paddle at last timestep
            collision_test = np.dot((self.b.pos - self.b.last_pos), (coll_p2 - self.b.pos))

            # print(f"change in position: {self.b.pos-self.b.last_pos}, collision: {collision_test}")
            if collision_test < 0 and np.dot(self.b.v, self.p2.normal) < 0:
                # move ball to collision point and reflect velocity from paddle plane
                self.b.pos = coll_p2
                self.b.last_pos = np.copy(self.b.pos)

                print("collision with paddle 2")

                self.b.v += -2*np.dot(self.b.v, self.p2.normal)*self.p2.normal


    # compute the point of collision with the line going through pos in the direction of v with the xz plane offset by y
    def vector_plane_collision(self, pos, v, y):
        x0 = pos[0]
        y0 = pos[1]
        z0 = pos[2]
        vx = v[0]
        vy = v[1]
        vz = v[2]

        return np.array(
            [
                (vx*y - vx*y0 + vy*x0)/vy, y, (vz*y + vy*z0 - vz*y0)/vy
            ])


def main():
    # timing
    t_start = datetime.now()
    t_last = datetime.now()

    # frame to be displayed
    frame = init_frame()

    # serial connection
    ser = init_serial()
    ack_received = False

    # animations
    animations = list()  # a list of all animations to be played
    animations.append(Pong())
    animations.append(Sphere(np.array([3.5, 3.5, 3.5], dtype=np.float32), 3))
    animations.append(Wave())
    animations.append(Cube())
    animations.append(Spiral())
    animations.append(Particles())
    animations.append(Rain())

    current_animation = 0  # list index of the currently selected animation

    # main loop
    while True:
        t_td = datetime.now() - t_start  # time elapsed since program start as datetime.timedelta
        t = ((t_td.days * 24 * 60 * 60 + t_td.seconds) * 1000 + t_td.microseconds / 1000) / 1000  # t_dt in seconds

        dt_td = datetime.now() - t_last  # time elapsed since t_last was updated
        dt = ((dt_td.days * 24 * 60 * 60 + dt_td.seconds) * 1000 + dt_td.microseconds / 1000) / 1000  # dt_td in seconds

        # compute new frames at a sufficiently low dt for simulations
        # (the display frequency is determined by the cube through the serial connection)
        if dt > 0.001:
            # compute current frame of the animation
            frame = animations[current_animation].compute_frame(t, dt)
            t_last = datetime.now()

        # send frame to arduino only if the arduino has sent an acknowledgement that it has read the previous data
        if ack_received:
            # print(f"{seconds:.2f}s: frame sent")
            send_frame(ser, frame)
            ack_received = False

        # check for acknowledgement from arduino
        ack = ser.read()
        if ack == b'\x06':
            # print("ACK received!")
            ack_received = True

        # change animation and reset timer periodically
        if t > 10:
            t_start = datetime.now()
            current_animation += 1

            # reset to first animation if all animations have been played
            if current_animation >= len(animations):
                current_animation = 0


# initialise a serial connection with the arduino and return an object for using the connection
def init_serial():
    ser = serial.Serial()
    ser.baudrate = 57600
    ser.port = 'COM6'
    ser.open()
    if ser.is_open is not True:
        raise RuntimeError("Could not open serial port")
    return ser


# Initialise an empty frame. This is a 8x8x8 matrix (3d array) whose all elements are initialised to zero.
# Each value corresponds to the on/off state of the voxels in the cube.
def init_frame():
    frame = []
    for z in range(8):
        layer = []
        for y in range(8):
            register = []
            for x in range(8):
                register.append(0)
            layer.append(register)
        frame.append(layer)
    return frame


# Send the frame frame over the serial connection ser.
# The data has to sent on 2 blocks because the arduino buffer can hold only 63 bytes
# To do this, send_frame fist sends half of the data and waits for the cube to read the data and request
# the other half with the byte 0x05.
def send_frame(ser, frame):
    # send first half of the data
    for z in range(4):
        for y in range(8):
            ser.write(struct.pack('>B', list_to_byte(frame[z][y])))

    # wait for the arduino to reply
    while not ser.in_waiting:
        pass
    response = ser.read()

    # check if cube requested for the other half of the data
    if response == b'\x05':
        # send the second half of the data
        for z in range(4, 8):
            for y in range(8):
                ser.write(struct.pack('>B', list_to_byte(frame[z][y])))
    else:
        ...


# convert list representing values in a register to the byte that should be written to the register
# i.e. convert an array containing 8 elements representing an on/off (1/0) state to a corresponding byte
def list_to_byte(register_list):
    byte = 0
    for i in range(8):
        if register_list[i] == 1:
            byte += 2**i
    return byte


# returns a scaling matrix about the origin for a scale 4-vector s
def scale_matrix(s):
    S = np.array([
        [s[0],     0,    0,  0],
        [   0,  s[1],    0,  0],
        [   0,     0, s[2],  0],
        [   0,     0,    0,  1]
    ], dtype=np.float32)
    return S


# returns a translation matrix for displacement 4-vector disp
def translation_matrix(disp):
    T = np.array([
        [1, 0, 0, disp[0]],
        [0, 1, 0, disp[1]],
        [0, 0, 1, disp[2]],
        [0, 0, 0,    1]
    ], dtype=np.float32)
    return T


# returns the 4x4 rotation matrix for the given euler angles
def rotation_matrix(yaw, pitch, roll):
    # rotation about the z axis
    Rz = np.array([
        [cos(yaw), -sin(yaw), 0, 0],
        [sin(yaw), cos(yaw), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ], dtype=np.float32)

    # rotation about the y axis
    Ry = np.array([
        [cos(pitch), 0, sin(pitch), 0],
        [0, 1, 0, 0],
        [-sin(pitch), 0, cos(pitch), 0],
        [0, 0, 0, 1]
    ], dtype=np.float32)

    # rotation about the x axis
    Rx = np.array([
        [1, 0, 0, 0],
        [0, cos(roll), -sin(roll), 0],
        [0, sin(roll), cos(roll), 0],
        [0, 0, 0, 1]
    ], dtype=np.float32)

    # apply the rotations in the order yaw, pitch, roll (from right to left)
    return Rx @ Ry @ Rz


if __name__ == '__main__':
    main()
